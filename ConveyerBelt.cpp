/**
 * @section assumptions Conveyor Belt Simulation Assumptions

 * @subsection Belt Movement
 * - A new component (or none) enters the first slot at each step's start.
 * - Items on the last slot are discarded if not collected.

 * @subsection Workers
 * - Workers only pick from the slot in front of them.
 * - Workers make use of commutative law: whether they have A,B as a pair or B,A is irrelevant
 * - They try to pick when not assembling and can hold two items max.
 * - Assembling workers don't interact with the belt.
 * - Only one worker in a pair will assemble at a time.
 * - Finished products are placed on the belt in front of them.

 * @subsection Item Handling
 * - Product assembly takes 4 cycles.
 * - A slot can't hold both a component and a product.

 * @subsection Component Probabilities
 * - The entry of components is determined by a static probability distribution and randomness.

 * @subsection Factory Specs
 * - Fixed number of belt slots and worker pairs per factory.

 * @subsection Initial Setup
 * - Workers start empty-handed, the belt is empty, and product/component counts are zero.

 * @subsection Probability Configuration
 * - Component probabilities must sum to 1 and are provided at runtime.
 */

#include <iostream>
#include <vector>
#include <map>
#include <functional>
#include <random>
#include <set>

 // Components and Products
enum class Component : std::uint8_t { None = 0, A, B, C };
enum class Product : std::uint8_t { None = 0, P, Q };


// map for adding flexibility for additional, product types or components
using Combination = std::set<Component>;
using CombinationLogic = std::map<Combination, Product>;
const CombinationLogic DEFAULT_LOGIC = {
	{{Component::A, Component::B}, Product::P},
	{{Component::A, Component::C}, Product::Q}
};



// assumption: makes only sense if you "review" that the probabilities add to 1.0; otherweise we need
// a check in the constructor and throw an exception if necessary. Or static_assert? but in a constexpr environment
using ProbabilitiesMap = std::map<Component, double>;
const std::map<Component, double> DEFAULT_PROBABILITIES = {
	{Component::None, 0.2f},
	{Component::A, 0.4f},
	{Component::B, 0.2f},
	{Component::C, 0.2f}
};

// Belt Slot
class Slot {
public:
	Component component{ Component::None };
	Product product{ Product::None };

	void setComponent(Component c) {
		component = c;
		product = Product::None;
	}
	void setProduct(Product p) {
		product = p;
		component = Component::None;
	}
};

// Worker
class Worker {
private:
	Component mLeftHand{ Component::None };
	Component mRightHand{ Component::None };
	Product mFinishedProduct{ Product::None }; // is set, in case the assembly time is over
	int mAssemblyTimeLeft{ 0 };
	static constexpr unsigned int cAssemblyTime = 3;

protected:
	Worker(const Worker& other) = default;
	Worker& operator=(const Worker& other) = default;
public:
	Worker() = default;
	// Checks if the worker can hold two components.
	Product getFinishedProduct() const { return mFinishedProduct; }

	bool emptyHands() const { return mLeftHand == Component::None && mRightHand == Component::None; }
	bool canPick() const { return mLeftHand == Component::None || mRightHand == Component::None; } // If one of the hands is free
	bool isAssembling() const { return mAssemblyTimeLeft > 0; }
	bool isHoldingProduct() const { return mFinishedProduct != Product::None; }
	void startAssembly() { mAssemblyTimeLeft = cAssemblyTime; }  // 3 steps as 4th? is placement
	std::pair<Component, Component> getHands() const {
		return { mLeftHand, mRightHand };
	}


	void pick(Component const& c) {
		if (mLeftHand == Component::None)
			mLeftHand = c;
		else if (mRightHand == Component::None)
			mRightHand = c;
		else {
			return; //both hands full
		}
	}

	Component predictivePick(Component const& current, CombinationLogic const& logic) {
		if (!canPick()) return Component::None;

		if (emptyHands()) {
			return current; // If holding nothing, always pick the current component
		}

		Component heldComponent = (mLeftHand != Component::None) ? mLeftHand : mRightHand;

		Combination potentialCombination = { heldComponent, current };

		if (logic.find(potentialCombination) != logic.end()) {
			return current;
		}

		return Component::None;  // If it doesn't complete a product, don't pick it up
	}

	/**
	 * @brief Attempts to assemble a product based on held components and provided logic.
	 *
	 * This method determines if the worker has the necessary components
	 * to assemble a product based on the provided map logic. If
	 * the components match any combination in the map, the worker will
	 * start the assembly process.
	 *
	 * @param logic The map containing combinations of components and the resulting product.
	 * @return true if the assembly process started, false otherwise.
	 */
	bool tryAssemble(const CombinationLogic& logic) {
		Combination combination = { mLeftHand, mRightHand };

		auto it = logic.find(combination);

		// If the components match a combination in the map
		if (it != logic.end()) {
			// Set the finished product based on the found combination
			mFinishedProduct = it->second;

			// Clear the worker's hands as the components are now used
			mLeftHand = mRightHand = Component::None;

			// Start the assembly process
			startAssembly();

			// Return true to indicate the assembly process has started
			return true;
		}

		// If no matching combination is found, return false
		return false;
	}

	Product giveProduct() {
		if (mAssemblyTimeLeft > 0) {
			mAssemblyTimeLeft--;
			if (mAssemblyTimeLeft == 0) {
				Product p = mFinishedProduct;
				mFinishedProduct = Product::None;
				return p;
			}
		}
		return Product::None;
	}
};

// Pair of workers representing two workers facing each other over a slot
using WorkerPair = std::pair<Worker, Worker>;



template<typename CombinationLogic, typename ProbabilitiesMap>
class Factory {
private:
	std::vector<Slot> mBelt;                        // The conveyor belt represented as a sequence of slots.
	std::vector<WorkerPair> mWorkerPairs;            // Worker pairs for each slot on the conveyor belt.
	const CombinationLogic& mCombinationLogic;       // Logic that defines which components combine to form products.
	const ProbabilitiesMap& mComponentProbabilities;

	std::map<Component, int> mUnprocessedComponents; // Map to track components that weren't picked up.
	int mFinishedProductsCount{ 0 };                 // Counter for finished products.


	char componentToChar(Component const& c) const {
		switch (c) {
		case Component::A: return 'A';
		case Component::B: return 'B';
		case Component::C: return 'C';
		default: return 'x';
		}
	}

	char productToChar(Product const& p) const {
		switch (p) {
		case Product::P: return 'P';
		case Product::Q: return 'Q';
		default: return 'x';
		}
	}

protected:
	Factory(const Factory& other) = default;             // Protected copy constructor
	Factory& operator=(const Factory& other) = default;  // Protected assignment operator

public:
	Factory() = default;
	Factory(std::uint8_t beltSize, std::uint8_t numWorkerPairs, const CombinationLogic& logic, const ProbabilitiesMap& probabilities)
		: mBelt(beltSize),
		mWorkerPairs(numWorkerPairs),
		mCombinationLogic(logic),
		mComponentProbabilities(probabilities) {

		// Check that there are not more worker pairs than slots
		if (mWorkerPairs.size() > mBelt.size()) {
			throw std::runtime_error("There are more worker pairs than slots on the belt.");
		}
		//here would be a good position to check whether all probabilities add up to 1.0

	}

	// Simulates the movement of the conveyor belt by shifting the contents to the right.
	void moveBelt() {
		for (int i = mBelt.size() - 1; i > 0; --i) { // let's look from right to left
			mBelt[i] = mBelt[i - 1]; // and copy the values from left to right
		}
		mBelt[0].setComponent(getRandomComponent()); // Fill the new slot with randomized component
	}

	Component getRandomComponent() {
		static constexpr double cDistMin = 0.0f;
		static constexpr double cDistMax = 1.0f;
		std::random_device rd;
		std::mt19937 mt(rd());
		std::uniform_real_distribution<double> dist(cDistMin, cDistMax);

		double prob = dist(mt);

		double cumulativeProbability = 0.0;
		for (const auto& entry : mComponentProbabilities) { // iterate in map
			cumulativeProbability += entry.second;
			if (prob <= cumulativeProbability) {     // if random number is in current component's interval, return that component
				return entry.first;
			}
		}
		return Component::None; // This should ideally never be reached if the probabilities sum up to 1
	}

	void handleComponentPickup(Worker& worker, Slot& slot) { // Process component pickup by a worker
		if (worker.isAssembling() || !worker.canPick()) return;
		Component desiredComponent = worker.predictivePick(slot.component, mCombinationLogic);
		if (desiredComponent != Component::None) {
			worker.pick(desiredComponent);
			slot.setComponent(Component::None);
		}
		else {}
	}

	bool handleAssembly(Worker& worker, Worker& other) { // Handle the assembly action of a worker.
		if (worker.tryAssemble(mCombinationLogic)) {
			other.pick(Component::None);  // Ensure the other worker doesn't interfere
			return true;
		}
		return false;
	}

	void handleProductPlacement(Worker& worker1, Worker& worker2, Slot& slot) {
		// Only attempt placement if the slot is empty
		if (slot.product == Product::None && slot.component == Component::None) {
			Product p1 = worker1.giveProduct();
			if (p1 != Product::None) {
				slot.setProduct(p1);
				mFinishedProductsCount++;
			}
			else {
				Product p2 = worker2.giveProduct();
				if (p2 != Product::None) {
					slot.setProduct(p2);
					mFinishedProductsCount++;
				}
			}
		}
		else {}
	}

	void simulate(std::uint8_t const& steps) {
		for (std::uint8_t step = 0; step < steps; ++step) {
			moveBelt();
			print(step);

			for (std::uint8_t i = 0; i < mWorkerPairs.size(); ++i) {
				Worker& worker1 = mWorkerPairs[i].first;
				Worker& worker2 = mWorkerPairs[i].second;
				Slot& slot = mBelt[i];

				handleComponentPickup(worker1, slot);
				if (!handleAssembly(worker1, worker2)) { // Only proceed if worker1 isn't assembling
					handleComponentPickup(worker2, slot);
					handleAssembly(worker2, worker1);
				}

				handleProductPlacement(worker1, worker2, slot);
			}

			// Count unprocessed components at the end of the belt
			if (mBelt.back().component != Component::None) {
				mUnprocessedComponents[mBelt.back().component]++;
			}
			else {}
		}
	}


	// Prints the current state of the conveyor belt in a ASCII-art style. Could be outsourced
	void print(std::uint8_t const& cycle) const {
		// Print top workers
		for (const auto& workerPair : mWorkerPairs) {
			if (workerPair.first.isAssembling()) {
				std::cout << "[" << productToChar(workerPair.first.getFinishedProduct()) << "] ";
			}
			else {
				std::pair<Component, Component> handsTop = workerPair.first.getHands();
				std::cout << " " << componentToChar(handsTop.first) << "v" << componentToChar(handsTop.second) << " ";
			}
		}
		std::cout << std::endl;

		for (size_t i = 0; i < mBelt.size(); ++i) { 		// Print top dashed line
			std::cout << "+---";
		}
		std::cout << "+\n";

		// Print conveyor belt content
		for (const Slot& slot : mBelt) {
			char slotChar = (slot.product != Product::None) ? productToChar(slot.product) : componentToChar(slot.component);
			std::cout << "| " << slotChar << " ";
		}
		std::cout << "|   CONVEYOR-BELT SIM-Cycle " << static_cast<int>(cycle) << "\n";

		for (size_t i = 0; i < mBelt.size(); ++i) { // Print dashed line
			std::cout << "+---";
		}
		std::cout << "+\n";

		// Print bottom workers
		for (const auto& workerPair : mWorkerPairs) {
			std::pair<Component, Component> handsBottom = workerPair.second.getHands();
			std::cout << " " << componentToChar(handsBottom.first) << "^" << componentToChar(handsBottom.second) << " ";
		}
		std::cout << "\n\n####################################\n" << std::endl;
	}


	// Display the results of the simulation
	void displayResults() const {
		std::cout << "Finished products: " << mFinishedProductsCount << std::endl;
		// Unprocessed components are the components that were left on the conveyor belt at the end of the simulation.
		for (const auto& pair : mUnprocessedComponents) {
			std::cout << "Unprocessed " << componentToChar(pair.first) << ": " << pair.second << std::endl;
		}
	}
};

int main() {
	try {
		Factory<CombinationLogic, ProbabilitiesMap> factory(10U, 5U, DEFAULT_LOGIC, DEFAULT_PROBABILITIES);
		factory.simulate(100);
		factory.displayResults();
	}
	catch (const std::exception& e) {
		std::cerr << "Error: " << e.what() << std::endl;
	}
	return 0;
}
